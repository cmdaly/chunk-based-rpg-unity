﻿using RobotArms;
using UnityEngine;

public enum Mutation
{
  Normal,
  Bear
}

public class Mutle : RobotArmsComponent
{
  [HideInInspector]
  public long ID = -1;
  public string Name = "Mutle";
  public Mutation Arms;
  public Mutation Feet;
  public Mutation Head;
  public Mutation Torso;
  public Mutation Wings;
}
