﻿using RobotArms;
using UnityEngine;

public enum Facing : int
{
  Down = 0,
  Left = 1,
  Up = 2,
  Right = 3
}

public class Direction : RobotArmsComponent
{
  public Facing Facing = Facing.Down;
}
