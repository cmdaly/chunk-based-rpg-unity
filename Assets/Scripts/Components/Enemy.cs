﻿using System;
using System.IO;
using RobotArms;
using UnityEngine;

[RequireComponent(typeof(PolyNavAgent), typeof(Animator), typeof(Direction))]
public class Enemy : RobotArmsComponent
{
  public Transform Victim;

  [HideInInspector] public int PathLifetime;
  [HideInInspector] public float AverageSpeed;
  [HideInInspector] public bool RecheckedPath;
  [HideInInspector] public int TimesRechecked;
}
