﻿using UnityEngine;

[System.Serializable]
public class SelectableJsonField
{
  public TextAsset Asset;
}
