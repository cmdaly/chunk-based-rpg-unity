﻿using RobotArms;
using UnityEngine;

class Velocity : RobotArmsComponent
{
  [HideInInspector] public Vector3 Vel = Vector3.zero;
}

