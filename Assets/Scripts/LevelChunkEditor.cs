﻿using System;
using SimpleJSON;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

[Flags]
public enum EditorListOption
{
  None = 0,
  ListSize = 1,
  ListLabel = 2,
  ElementLabels = 4,
  Buttons = 8,
  Default = ListSize | ListLabel | ElementLabels,
  NoElementLabels = ListSize | ListLabel,
  All = Default | Buttons
}

[CustomEditor(typeof (LevelChunk))]
public class LevelChunkEditor : Editor
{
  private bool _showPrefabs = false;

  /*private void OnEnable()
  {
    if (SceneView.onSceneGUIDelegate != DrawStuff)
    {
      SceneView.onSceneGUIDelegate += DrawStuff;
    }
  }

  public void OnDisable()
  {
    if (SceneView.onSceneGUIDelegate == DrawStuff)
    {
      SceneView.onSceneGUIDelegate -= DrawStuff;
    }
  }*/

  public override void OnInspectorGUI()
  {
    LevelChunk chunk = (LevelChunk) target;

    TextAsset jsonFile = EditorGUILayout.ObjectField("JSON File", chunk.JsonFile, typeof(TextAsset), false) as TextAsset;
    chunk.OutlineColor = EditorGUILayout.ColorField("Outline Color:", chunk.OutlineColor);

    if (jsonFile != chunk.JsonFile)
    {
      chunk.JsonFile = jsonFile;

      LevelUtility.LoadLevelData(chunk);
    }

    EditorGUILayout.LabelField("Width: " + chunk.Width);
    EditorGUILayout.LabelField("Height: " + chunk.Height);

    _showPrefabs = EditorGUILayout.Foldout(_showPrefabs, "Prefabs");
    if (_showPrefabs)
    {
      EditorGUILayout.BeginVertical();

      for (int i = 0; i < chunk.PrefabTypes.Count; i++)
      {
        string prefabType = chunk.PrefabTypes[i];
        Vector3 prefabPos = chunk.PrefabPositions[i];
        Color prefabColor = chunk.PrefabColors[i];

        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField(prefabType, GUILayout.Width(130));
        EditorGUILayout.LabelField(String.Format("x: {0}, y: {1}, z: {2}", prefabPos.x, prefabPos.y, prefabPos.z), GUILayout.Width(130));
        GUILayout.FlexibleSpace();
        chunk.PrefabColors[i] = EditorGUILayout.ColorField(prefabColor, GUILayout.Width(45));

        EditorGUILayout.EndHorizontal();
      }

      EditorGUILayout.EndVertical();
    }

    if (GUILayout.Button("Reload Level"))
    {
      LevelUtility.LoadLevelData(chunk);
    }

    EditorUtility.SetDirty(target);
  }

  private void OnSceneGui()
  {
  }

  private void DrawStuff(SceneView view)
  {
    LevelChunk chunk = (LevelChunk)target;

    if (chunk.JsonFile == null)
    {
      return;
    }
    else if (chunk.RootNode == null)
    {
      LevelUtility.LoadLevelData(chunk);
    }

    Vector3 chunkPos = chunk.gameObject.transform.position;

    float halfWidth = chunk.Width / 2;
    float halfHeight = chunk.Height / 2;

    Vector3[] verts =
    {
      new Vector3(chunkPos.x - halfWidth, chunkPos.y - halfHeight, chunkPos.z),
      new Vector3(chunkPos.x + halfWidth, chunkPos.y - halfHeight, chunkPos.z),
      new Vector3(chunkPos.x + halfWidth, chunkPos.y + halfHeight, chunkPos.z),
      new Vector3(chunkPos.x - halfWidth, chunkPos.y + halfHeight, chunkPos.z)
    };

    Handles.color = chunk.OutlineColor;
    for (int i = 0; i < verts.Length; i++)
    {
      Handles.DrawLine(verts[i], verts[(i + 1) % verts.Length]);
    }

    for (int i = 0; i < chunk.PrefabPositions.Count; i++)
    {
      Vector3 prefabPos = chunk.PrefabPositions[i];
      Color prefabColor = chunk.PrefabColors[i];
      string prefabType = chunk.PrefabTypes[i];

      GUIStyle style = new GUIStyle();
      style.normal.textColor = prefabColor;

      Vector2 pos2D = HandleUtility.WorldToGUIPoint(prefabPos);

      //Handles.color = prefabColor;
      //Handles.DrawSolidDisc(prefabPos, Vector3.forward, 30);
      //GUILayout.BeginArea(new Rect(pos2D.x, pos2D.y, 100, 100), prefabType, style);
      //GUILayout.EndArea();
      Handles.BeginGUI();
      GUI.Label(new Rect(pos2D.x, pos2D.y, 100, 100), prefabType, style);
      Handles.EndGUI();
      //Handles.Label(prefabPos, prefabType);
    }
  }
}
