﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof (FollowPlayer))]
public class FollowPlayerEditor : Editor
{
  public override void OnInspectorGUI()
  {
    FollowPlayer followPlayer = (FollowPlayer) target;
    
    followPlayer.Target = EditorGUILayout.ObjectField("Target", followPlayer.Target, typeof(Transform), true) as Transform;

    EditorGUILayout.LabelField("Left Boundary: " + followPlayer.LeftBound);
    EditorGUILayout.LabelField("Right Boundary: " + followPlayer.RightBound);
    EditorGUILayout.LabelField("Top Boundary: " + followPlayer.TopBound);
    EditorGUILayout.LabelField("Bottom Boundary: " + followPlayer.BottomBound);

    EditorGUILayout.LabelField("Width: " + followPlayer.Width);
    EditorGUILayout.LabelField("Height: " + followPlayer.Height);

    EditorUtility.SetDirty(followPlayer);
  }

  private void OnSceneGUI()
  {
    FollowPlayer followPlayer = (FollowPlayer) target;
    Vector3 position = followPlayer.gameObject.transform.position;

    Handles.color = Color.cyan;

    Vector3[] verts =
    {
      new Vector3(followPlayer.LeftBound, followPlayer.TopBound, position.z),
      new Vector3(followPlayer.RightBound, followPlayer.TopBound, position.z),
      new Vector3(followPlayer.RightBound, followPlayer.BottomBound, position.z),
      new Vector3(followPlayer.LeftBound, followPlayer.BottomBound, position.z)
    };

    for (int i = 0; i < verts.Length; i++)
    {
      Handles.DrawLine(verts[i], verts[(i + 1) % verts.Length]);
    }

    float height = Mathf.Abs(followPlayer.TopBound - followPlayer.BottomBound);
    float width = Mathf.Abs(followPlayer.LeftBound - followPlayer.RightBound);
    float halfHeight = height / 2;
    float halfWidth = width / 2;

    Vector3 leftBoundHandlePos = new Vector3(followPlayer.LeftBound, followPlayer.TopBound + halfHeight, 0);
    Vector3 rightBoundHandlePos = new Vector3(followPlayer.RightBound, followPlayer.TopBound + halfHeight, 0);
    Vector3 topBoundHandlePos = new Vector3(followPlayer.LeftBound + halfWidth, followPlayer.TopBound, 0);
    Vector3 bottomBoundHandlePos = new Vector3(followPlayer.LeftBound + halfWidth, followPlayer.BottomBound, 0);

    Handles.color = Color.white;

    leftBoundHandlePos = Handles.Slider2D(leftBoundHandlePos, new Vector3(1, 0, 0), new Vector3(1, 0, 0), new Vector3(0, 1, 0), 10, Handles.CubeCap, 1);
    rightBoundHandlePos = Handles.Slider2D(rightBoundHandlePos, new Vector3(1, 0, 0), new Vector3(1, 0, 0), new Vector3(0, 1, 0), 10, Handles.CubeCap, 1);
    topBoundHandlePos = Handles.Slider2D(topBoundHandlePos, new Vector3(1, 0, 0), new Vector3(1, 0, 0), new Vector3(0, 1, 0), 10, Handles.CubeCap, 1);
    bottomBoundHandlePos = Handles.Slider2D(bottomBoundHandlePos, new Vector3(1, 0, 0), new Vector3(1, 0, 0), new Vector3(0, 1, 0), 10, Handles.CubeCap, 1);

    if (GUI.changed)
    {
      followPlayer.LeftBound = Mathf.Min(leftBoundHandlePos.x, followPlayer.RightBound);
      followPlayer.RightBound = Mathf.Max(rightBoundHandlePos.x, followPlayer.LeftBound);
      followPlayer.TopBound = Mathf.Min(topBoundHandlePos.y, followPlayer.BottomBound);
      followPlayer.BottomBound = Mathf.Max(bottomBoundHandlePos.y, followPlayer.TopBound);

      followPlayer.Width = Mathf.Abs(followPlayer.LeftBound - followPlayer.RightBound);
      followPlayer.Height = Mathf.Abs(followPlayer.TopBound - followPlayer.BottomBound);

      EditorUtility.SetDirty(target);
    }
  }
}
