﻿using System.Collections.Generic;
using RobotArms;
using SimpleJSON;
using UnityEditor;
using UnityEngine;

public class LevelChunk : RobotArmsComponent
{
  //public string JsonFile = null;
  public TextAsset JsonFile = null;
  public JSONNode RootNode = null;

#if UNITY_EDITOR
  public Color OutlineColor = Color.cyan;

  public List<Vector3> PrefabPositions = new List<Vector3>();
  public List<Color> PrefabColors = new List<Color>();
  public List<string> PrefabTypes = new List<string>();
#endif

  public string Name = "";

  public float Width = 600;
  public float Height = 600;

  public bool IsLoaded = false;

  public List<GameObject> Objects = new List<GameObject>();

#if UNITY_EDITOR
  private void OnDrawGizmos()
  {
    LevelChunk chunk = this;

    if (chunk.JsonFile == null)
    {
      return;
    }
    else if (chunk.RootNode == null)
    {
      LevelUtility.LoadLevelData(chunk);
    }

    Vector3 chunkPos = chunk.gameObject.transform.position;

    float halfWidth = chunk.Width / 2;
    float halfHeight = chunk.Height / 2;

    Vector3[] verts =
    {
      new Vector3(chunkPos.x - halfWidth, chunkPos.y - halfHeight, chunkPos.z),
      new Vector3(chunkPos.x + halfWidth, chunkPos.y - halfHeight, chunkPos.z),
      new Vector3(chunkPos.x + halfWidth, chunkPos.y + halfHeight, chunkPos.z),
      new Vector3(chunkPos.x - halfWidth, chunkPos.y + halfHeight, chunkPos.z)
    };

    Handles.color = chunk.OutlineColor;
    for (int i = 0; i < verts.Length; i++)
    {
      Handles.DrawLine(verts[i], verts[(i + 1) % verts.Length]);
    }

    for (int i = 0; i < chunk.PrefabPositions.Count; i++)
    {
      Vector3 prefabPos = chunk.PrefabPositions[i];
      Color prefabColor = chunk.PrefabColors[i];
      string prefabType = chunk.PrefabTypes[i];

      GUIStyle style = new GUIStyle();
      style.normal.textColor = prefabColor;

      Vector2 pos2D = HandleUtility.WorldToGUIPoint(prefabPos);

      //Handles.color = prefabColor;
      //Handles.DrawSolidDisc(prefabPos, Vector3.forward, 30);
      //GUILayout.BeginArea(new Rect(pos2D.x, pos2D.y, 100, 100), prefabType, style);
      //GUILayout.EndArea();
      Handles.BeginGUI();
      GUI.Label(new Rect(pos2D.x, pos2D.y, 100, 100), prefabType, style);
      Handles.EndGUI();
      //Handles.Label(prefabPos, prefabType);
    }
  }
#endif
}
