﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using RobotArms;
using SimpleJSON;
using UnityEngine;

[ProcessorOptions(-30)]
class LevelChunkProcessor : RobotArmsProcessor<LevelChunk>
{
  private MainController _controller;
  private Thread _thread;
  private Queue<LevelChunk> _chunks = new Queue<LevelChunk>();

  public override void Initialize(GameObject entity, LevelChunk chunk)
  {
    chunk.RootNode = JSON.Parse(chunk.JsonFile.text);

    chunk.Name = chunk.RootNode["name"];

    float width = chunk.RootNode["width"].AsFloat;
    float height = chunk.RootNode["height"].AsFloat;

    chunk.Width = width;
    chunk.Height = height;

#if UNITY_EDITOR
    Vector3 chunkPos = chunk.gameObject.transform.position;

    chunk.PrefabPositions.Clear();
    chunk.PrefabColors.Clear();
    chunk.PrefabTypes.Clear();

    JSONArray allPrefabs = chunk.RootNode["level"]["prefabs"].AsArray;

    foreach (JSONNode prefab in allPrefabs)
    {
      JSONArray position = prefab["position"].AsArray;
      Vector3 posVec = new Vector3(position[0].AsFloat, position[1].AsFloat, 0);
      posVec += chunkPos;

      chunk.PrefabPositions.Add(posVec);

      Color prefabColor = new Color(Random.value, Random.value, Random.value, 1.0f);
      chunk.PrefabColors.Add(prefabColor);

      chunk.PrefabTypes.Add(prefab["type"]);
    }
#endif
  }

  public override void Process(GameObject entity, LevelChunk chunk)
  {
    if (chunk.IsLoaded)
    {
      return;
    }

    Player[] allPlayers = RobotArmsUtils.GetAllComponentsOfType<Player>();

    foreach (Player player in allPlayers)
    {
      if (!player.isActiveAndEnabled)
      {
        continue;
      }

      Vector3 chunkPos = chunk.gameObject.transform.position;
      Vector3 actorPos = player.gameObject.transform.position;
      Vector3 dif = chunkPos - actorPos;

      float chunkHalfWidth = chunk.Width / 2;
      float chunkHalfHeight = chunk.Height / 2;

      bool inLoadRange = (Mathf.Abs(dif.x) < chunkHalfWidth + 300 && Mathf.Abs(dif.y) < chunkHalfHeight + 300);

      if (inLoadRange)
      {
        //AddChunk(chunk);
        if (_controller == null)
        {
          _controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<MainController>();
        }

        _controller.StartChildCoroutine(AddChunk(chunk));
      }
    }
  }

  private IEnumerator AddChunk(LevelChunk chunk)
  {
    if (_chunks.Count >= 5)
    {
      RemoveChunk(_chunks.Dequeue());
    }

    _chunks.Enqueue(chunk);
    chunk.IsLoaded = true;

    JSONArray allPrefabs = chunk.RootNode["level"]["prefabs"].AsArray;

    foreach (JSONNode prefab in allPrefabs)
    {
      yield return prefab;

      GameObject obj = TrashMan.spawn(prefab["type"]);
      Transform transform = obj.transform;

      JSONArray position = prefab["position"].AsArray;

      transform.parent = chunk.transform;
      transform.localPosition = Vector3.zero;
      transform.localPosition += new Vector3(position[0].AsFloat, position[1].AsFloat, 0);
    }
    /*if (_controller == null)
    {
      _controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<MainController>();
    }

    _controller.StartChildCoroutine(AddPrefabs(chunk, allPrefabs));*/
    /*_thread = new Thread(() => AddPrefabs(chunk, allPrefabs));
    _thread.Start();*/

    PolyNav2D.current.GenerateMap(false);
    //PolyNav2D.current.regenerateFlag = true;
  }

  private void RemoveChunk(LevelChunk chunk)
  {
    foreach (GameObject obj in chunk.Objects)
    {
      TrashMan.despawn(obj);
    }
  }
}
