﻿/*using System;
using RobotArms;
using UnityEngine;

[ProcessorOptions(-1)]
public class EnemyProcessor : RobotArmsProcessor<Enemy, TrashManRecyclableObject>
{
  private static GameObject _currentEntity;
  private static Enemy _currentEnemy;

  public override void Initialize(GameObject entity, Enemy enemy, TrashManRecyclableObject trashMan)
  {
    _currentEntity = entity;
    _currentEnemy = enemy;

    FollowVictim();
    trashMan.onSpawnedEvent += FollowVictim;
  }

  public override void Process(GameObject entity, Enemy enemy, TrashManRecyclableObject trashMan)
  {
    _currentEntity = entity;
    _currentEnemy = enemy;

    if (enemy.Agent.hasPath)
    {
      bool isVertical = (Math.Abs(enemy.Agent.movingDirection.y) > Math.Abs(enemy.Agent.movingDirection.x));

      if (isVertical)
      {
        enemy.Direction.Dir = (enemy.Agent.movingDirection.y < 0 ? Facing.Up : Facing.Down);
      }
      else
      {
        enemy.Direction.Dir = (enemy.Agent.movingDirection.x < 0 ? Facing.Left : Facing.Right);
      }

      enemy.Animator.SetBool("Idle", false);
      enemy.Animator.SetInteger("Facing", (int) enemy.Direction.Dir);
      enemy.Animator.SetFloat("Horizontal Direction", enemy.Agent.movingDirection.x);
      enemy.Animator.SetFloat("Vertical Direction", enemy.Agent.movingDirection.y);

      enemy.PathLifetime++;

      if (enemy.PathLifetime > 10 && enemy.PathLifetime % 2 == 0)
      {
        enemy.AverageSpeed += enemy.Agent.currentSpeed;
        enemy.AverageSpeed /= 2;
      }

      if (enemy.AverageSpeed < 2.5f)
      {
        float distanceRemaining = enemy.Agent.remainingDistance;

        enemy.Agent.Stop();
        OnReachedDestination(distanceRemaining < 25.0f);
      }
    }
    else
    {
      enemy.Animator.SetBool("Idle", true);
    }
  }

  static private void FollowVictim()
  {
    if (_currentEnemy.Victim)
    {
      _currentEnemy.PathLifetime = 0;
      _currentEnemy.AverageSpeed = _currentEnemy.Agent.maxSpeed;
      _currentEnemy.RecheckedPath = false;
      _currentEnemy.TimesRechecked = 0;

      _currentEnemy.Agent.SetDestination(_currentEnemy.Victim.position, OnReachedDestination);
    }
  }

  static private void OnReachedDestination(bool success)
  {
    if (success)
    {
      Despawn();
    }
    else
    {
      if (!_currentEnemy.RecheckedPath && _currentEnemy.TimesRechecked < 5)
      {
        _currentEnemy.RecheckedPath = !_currentEnemy.Agent.SetDestination(_currentEnemy.Victim.position, OnReachedDestination);
        _currentEnemy.TimesRechecked++;
      }
      else
      {
        Despawn();
      }
    }

    //Debug.Log(this.name + (success ? " reached " : " failed to reach") + " its destination.");
  }

  static private void Despawn()
  {
    _currentEnemy.PathLifetime = 0;
    _currentEnemy.AverageSpeed = _currentEnemy.Agent.maxSpeed;
    _currentEnemy.RecheckedPath = false;
    _currentEnemy.TimesRechecked = 0;

    TrashMan.despawn(_currentEntity);
  }
}
*/