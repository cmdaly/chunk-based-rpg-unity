﻿using RobotArms;
using UnityEngine;

class MutleProcessor : RobotArmsProcessor<Mutle, TrashManRecyclableObject>
{
  private GameObject _controller;

  private GameObject _currentEntity;
  private Mutle _currentMutle;

  public override void Initialize(GameObject entity, Mutle mutle, TrashManRecyclableObject trashMan)
  {
    _controller = GameObject.FindGameObjectWithTag("GameController");

    _currentEntity = entity;
    _currentMutle = mutle;

    trashMan.onSpawnedEvent += MutleAdded;
    trashMan.onDespawnedEvent += MutleRemoved;
  }

  private void MutleAdded()
  {
    _controller.SendMessage("MutleAdded", _currentMutle);
  }

  private void MutleRemoved()
  {
    _controller.SendMessage("MutleRemoved", _currentMutle);
  }
}
