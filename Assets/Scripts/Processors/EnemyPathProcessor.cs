﻿using RobotArms;
using UnityEngine;

[ProcessorOptions(-1)]
internal class EnemyPathProcessor : RobotArmsProcessor<Enemy, PolyNavAgent, TrashManRecyclableObject>
{
  private static GameObject _currentEntity;
  private static Enemy _currentEnemy;
  private static PolyNavAgent _currentAgent;

  public override void Initialize(GameObject entity, Enemy enemy, PolyNavAgent agent, TrashManRecyclableObject trashMan)
  {
    _currentEntity = entity;
    _currentEnemy = enemy;
    _currentAgent = agent;

    /*FollowVictim();
    trashMan.onSpawnedEvent += FollowVictim;*/
    trashMan.onDespawnedEvent += Despawn;
  }

  public override void Process(GameObject entity, Enemy enemy, PolyNavAgent agent, TrashManRecyclableObject trashMan)
  {
    if (!entity.activeInHierarchy)
    {
      return;
    }
    
    _currentEntity = entity;
    _currentEnemy = enemy;
    _currentAgent = agent;

    if (agent.hasPath)
    {
      enemy.PathLifetime++;

      if (enemy.PathLifetime > 10 && enemy.PathLifetime % 2 == 0)
      {
        enemy.AverageSpeed += agent.currentSpeed;
        enemy.AverageSpeed /= 2;
      }

      if (enemy.AverageSpeed < 2.5f)
      {
        float distanceRemaining = agent.remainingDistance;

        agent.Stop();
        OnReachedDestination(distanceRemaining < 25.0f);
      }
    }
    else if (Input.GetKeyUp(KeyCode.Space))
    {
      FollowVictim();
    }
  }

  static private void FollowVictim()
  {
    if (_currentEnemy.Victim)
    {
      _currentEnemy.PathLifetime = 0;
      _currentEnemy.AverageSpeed = _currentAgent.maxSpeed;
      _currentEnemy.RecheckedPath = false;
      _currentEnemy.TimesRechecked = 0;

      _currentAgent.SetDestination(_currentEnemy.Victim.position, OnReachedDestination);
    }
  }

  static private void OnReachedDestination(bool success)
  {
    if (success)
    {
      TrashMan.despawn(_currentEntity);
    }
    else
    {
      if (!_currentEnemy.RecheckedPath && _currentEnemy.TimesRechecked < 5)
      {
        _currentEnemy.RecheckedPath = !_currentAgent.SetDestination(_currentEnemy.Victim.position, OnReachedDestination);
        _currentEnemy.TimesRechecked++;
      }
      else
      {
        TrashMan.despawn(_currentEntity);
      }
    }

    //Debug.Log(this.name + (success ? " reached " : " failed to reach") + " its destination.");
  }

  static private void Despawn()
  {
    _currentAgent.Stop();

    _currentEnemy.PathLifetime = 0;
    _currentEnemy.AverageSpeed = _currentAgent.maxSpeed;
    _currentEnemy.RecheckedPath = false;
    _currentEnemy.TimesRechecked = 0;
  }
}
