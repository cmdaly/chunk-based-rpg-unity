﻿using System;
using RobotArms;
using UnityEngine;

[ProcessorOptions(-3)]
internal class EnemyDirectionProcessor : RobotArmsProcessor<Enemy, Direction, PolyNavAgent>
{
  public override void Process(GameObject entity, Enemy enemy, Direction direction, PolyNavAgent agent)
  {
    if (!entity.activeInHierarchy)
    {
      return;
    }
    
    if (agent.hasPath)
    {
      var isVertical = (Math.Abs(agent.movingDirection.y) > Math.Abs(agent.movingDirection.x));

      if (isVertical)
      {
        direction.Facing = (agent.movingDirection.y < 0 ? Facing.Up : Facing.Down);
      }
      else
      {
        direction.Facing = (agent.movingDirection.x < 0 ? Facing.Left : Facing.Right);
      }
    }
  }
}
