﻿using RobotArms;
using UnityEngine;

class PlayerInputProcessor : RobotArmsProcessor<Player, PlayerInput, Velocity>
{
  private GameObject _currentEntity;
  private Player _currentPlayer;

  /*public override void Initialize(GameObject entity, Player player, Velocity velocity)
  {
    _currentEntity = entity;
    _currentPlayer = player;
  }*/

  public override void Process(GameObject entity, Player player, PlayerInput input, Velocity velocity)
  {
    if (Input.GetKey(KeyCode.A))
    {
      velocity.Vel.x = -5;
      velocity.Vel.y = 0;
    }
    else if (Input.GetKey(KeyCode.D))
    {
      velocity.Vel.x = 5;
      velocity.Vel.y = 0;
    }
    else if (Input.GetKey(KeyCode.W))
    {
      velocity.Vel.x = 0;
      velocity.Vel.y = 5;
    }
    else if (Input.GetKey(KeyCode.S))
    {
      velocity.Vel.x = 0;
      velocity.Vel.y = -5;
    }
    else
    {
      velocity.Vel.x = 0;
      velocity.Vel.y = 0;
    }
  }
}
