﻿using RobotArms;
using UnityEngine;

class MovementProcessor : RobotArmsProcessor<Player, Velocity>
{
  private GameObject _currentEntity;
  private Player _currentPlayer;

  /*public override void Initialize(GameObject entity, Player player, Velocity velocity)
  {
    _currentEntity = entity;
    _currentPlayer = player;
  }*/

  public override void Process(GameObject entity, Player player, Velocity velocity)
  {
    entity.transform.position += velocity.Vel;
  }
}
