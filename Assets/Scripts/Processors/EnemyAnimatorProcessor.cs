﻿using RobotArms;
using UnityEngine;

[ProcessorOptions(-2)]
internal class EnemyAnimatorProcessor : RobotArmsProcessor<Enemy, RobotAnimator, Direction, PolyNavAgent>
{
  public override void Initialize(GameObject entity, Enemy enemy, RobotAnimator robotAnimator, Direction direction, PolyNavAgent agent)
  {
    robotAnimator.Animator = entity.GetComponent<Animator>();
  }

  public override void Process(GameObject entity, Enemy enemy, RobotAnimator robotAnimator, Direction direction, PolyNavAgent agent)
  {
    if (!entity.activeInHierarchy)
    {
      return;
    }

    var animator = robotAnimator.Animator;

    if (agent.hasPath)
    {
      animator.SetBool("Idle", false);
      animator.SetInteger("Facing", (int) direction.Facing);
      animator.SetFloat("Horizontal Direction", agent.movingDirection.x);
      animator.SetFloat("Vertical Direction", agent.movingDirection.y);
    }
    else
    {
      animator.SetBool("Idle", true);
    }
  }
}
