﻿using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
  public float LeftBound = -50;
  public float RightBound = 50;
  public float TopBound = -50;
  public float BottomBound = 50;

  public float Width = 0;
  public float Height = 0;

  public Transform Target = null;

  private float _actualLeftBound;
  private float _actualRightBound;
  private float _actualTopBound;
  private float _actualBottomBound;

  // Use this for initialization
  private void Start()
  {
    float vertExtent = Camera.main.orthographicSize;
    float horzExtent = vertExtent * Screen.width / Screen.height;
    
    _actualLeftBound = LeftBound + horzExtent;
    _actualRightBound = RightBound - horzExtent;
    _actualTopBound = (vertExtent - Height / 2);
    _actualBottomBound = (Height / 2 - vertExtent);
  }

  // Update is called once per frame
  private void Update()
  {
    var pos = new Vector3(Target.position.x, Target.position.y, transform.position.z);
    pos.x = Mathf.Clamp(pos.x, _actualLeftBound, _actualRightBound);
    pos.y = Mathf.Clamp(pos.y, _actualBottomBound, _actualTopBound);
    transform.position = pos;
  }
}
