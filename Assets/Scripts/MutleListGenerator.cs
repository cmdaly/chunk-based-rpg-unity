﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class Item
{
  public string Name;
    public Button.ButtonClickedEvent OnSubmit;
}

public class MutleListGenerator : MonoBehaviour
{
  public GameObject BasePanel;
  public GameObject ShowPanelButton;

  public GameObject BaseItem;
  public List<Item> ItemList;

  public Transform ContentPanel;

  // Use this for initialization
  private void Start()
  {
    PopulateList();
  }

  private void PopulateList()
  {
    foreach (Item item in ItemList)
    {
      GameObject listItem = Instantiate(BaseItem) as GameObject;
      BaseItem baseItem = listItem.GetComponent<BaseItem>();
      baseItem.NameLabel.text = item.Name;
      baseItem.Button.onClick = item.OnSubmit;
      baseItem.transform.SetParent(ContentPanel);
    }
  }

  public void MutleSelected(string message)
  {
    Debug.Log(message);
  }
  
  public void OpenListMenu()
  {
    BasePanel.SetActive(true);
    ShowPanelButton.SetActive(false);
  }

  public void CloseListMenu()
  {
    BasePanel.SetActive(false);
    ShowPanelButton.SetActive(true);
  }
}
