﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainController : MonoBehaviour
{
  public readonly List<Mutle> Mutles = new List<Mutle>();
  private AsyncOperation _async;

  // Use this for initialization
  private void Start()
  {
    enabled = false;
  }

  public void Update()
  {
    /*if (Input.GetKeyUp(KeyCode.A) && _async == null)
    {
      _async = Application.LoadLevelAdditiveAsync("LevelAdd");
    }

    if (_async != null && _async.isDone)
    {
      _async = null;
    }*/
  }

  public void StartChildCoroutine(IEnumerator coroutineMethod)
  {
    StartCoroutine(coroutineMethod);
  }

  public void MutleAdded(Mutle mutle)
  {
    Mutles.Add(mutle);
  }

  public void MutleRemoved(Mutle mutle)
  {
    Mutles.Remove(mutle);
  }
}
