﻿using UnityEngine;

public class Spawner : MonoBehaviour
{
  private Camera _camera;
  private GameObject _treeParent;
  private GameObject _enemyParent;
  private GameObject _player;

  // Use this for initialization
  private void Start()
  {
    //_camera = tk2dCamera.Instance.GetComponent<Camera>();
    _camera = Camera.main;
    _treeParent = GameObject.Find("Trees");
    _enemyParent = GameObject.Find("Enemies");
    _player = GameObject.Find("Player");
  }

  // Update is called once per frame
  private void Update()
  {

    if (Input.GetKey(KeyCode.T))
    {
      if (Input.GetMouseButtonUp(0))
      {
        GameObject tree = TrashMan.spawn("Tree (1)");
        Transform treeTransform = tree.transform;

        Vector3 pos = _camera.ScreenToWorldPoint(Input.mousePosition);
        pos.z = 1;

        treeTransform.parent = _treeParent.transform;
        treeTransform.localPosition = pos;
        treeTransform.localScale = new Vector3(1, 1, 1);
      }
      else if (Input.GetMouseButtonUp(1))
      {
        GameObject tree = TrashMan.spawn("Tree (2)");
        Transform treeTransform = tree.transform;

        Vector3 pos = _camera.ScreenToWorldPoint(Input.mousePosition);
        pos.z = 1;

        treeTransform.parent = _treeParent.transform;
        treeTransform.localPosition = pos;
        treeTransform.localScale = new Vector3(1, 1, 1);
      }
    }
    else if (Input.GetMouseButtonUp(1))
    {
      GameObject enemy = TrashMan.spawn("Enemy");
      Transform enemyTransform = enemy.transform;

      Vector3 pos = _camera.ScreenToWorldPoint(Input.mousePosition);
      pos.z = 1;

      enemyTransform.parent = _enemyParent.transform;
      enemyTransform.localPosition = pos;

      Enemy enemyComponent = enemy.GetComponent<Enemy>();
      enemyComponent.Victim = _player.transform;
    }
  }
}
