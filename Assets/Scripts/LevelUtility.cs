﻿using SimpleJSON;
using UnityEngine;

class LevelUtility
{
  public static void LoadLevelData(LevelChunk chunk)
  {
    if (chunk.JsonFile != null)
    {
      chunk.RootNode = JSON.Parse(chunk.JsonFile.text);

      chunk.Name = chunk.RootNode["name"];

      float width = chunk.RootNode["width"].AsFloat;
      float height = chunk.RootNode["height"].AsFloat;

      chunk.Width = width;
      chunk.Height = height;

      Vector3 chunkPos = chunk.gameObject.transform.position;

      chunk.PrefabPositions.Clear();
      chunk.PrefabColors.Clear();
      chunk.PrefabTypes.Clear();

      JSONArray allPrefabs = chunk.RootNode["level"]["prefabs"].AsArray;

      foreach (JSONNode prefab in allPrefabs)
      {
        JSONArray position = prefab["position"].AsArray;
        Vector3 posVec = new Vector3(position[0].AsFloat, position[1].AsFloat, 0);
        posVec += chunkPos;

        chunk.PrefabPositions.Add(posVec);

        Color prefabColor = new Color(Random.value, Random.value, Random.value, 1.0f);
        chunk.PrefabColors.Add(prefabColor);

        chunk.PrefabTypes.Add(prefab["type"]);
      }
    }
  }
}
