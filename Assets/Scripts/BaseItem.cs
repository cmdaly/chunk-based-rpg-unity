﻿using UnityEngine;
using UnityEngine.UI;

public class BaseItem : MonoBehaviour
{
  public Button Button;
  public Text NameLabel;
}
